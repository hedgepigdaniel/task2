/*
 * numberTest.h
 * COMP1917 task 2
 * Daniel Playfair Cal and Natalie Wyburn
 * 03/04/2012
 */

/*
 * This file defines functions for testing if a number belongs to the
 * mandelbrot set.
 */

/*
 * function: stepsFromNumber
 * given the coefficients of 1 and i of a complex number, this function
 * determines whether or not the number belongs to the mandelbrot set.
 * If the number is not part of the set it returns a number 1 <= number
 * <= 255, which is the number of steps used. Otherwise, the function
 * returns 0.
 */

#define MAX_STEPS 255

unsigned char stepsFromNumber(double realPart, double imaginaryPart);