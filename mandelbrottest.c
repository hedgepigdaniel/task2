// Daniel Playfair Cal
// 12/04/2012

#include <stdio.h>
#include <stdlib.h>

#include "mandelbrot.h"


int main(int argc, char* argv[]) {
    // print out the mandelbrot set
    ComplexNumber z;

    for (z.Im = 1; z.Im >= -1; z.Im -= 0.03) {
        for (z.Re = -2; z.Re <= 1; z.Re += 0.02) {
            if (mandelbrotIterations(z) == MAX_ITERATIONS) {
                printf("#");
            } else {
                printf(".");
            }
        }
        printf("\n");
    }

    return EXIT_SUCCESS;
}