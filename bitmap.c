/*
 * bitmap.c
 * Daniel Playfair Cal & Natalie Wyburn
 * 04/04/2012
 * Provides functions for creating bitmap images
 */

#include "bitmap.h"
#include "endian.h"

#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>

// This prevents automatic padding on the structs which break the file
// format
#pragma pack(push, 1)

// Bitmap files header - the first part of the file with basic info
typedef struct _FileHeader {
    char magic1;
    char magic2;
    uint32_t fileSize;
    uint16_t creator1;
    uint16_t creator2;
    uint32_t imageOffset;
} FileHeader;

// DIB Header - comes after file header and defines the image properties
// This header is of the type BITMAPCOREHEADER (the simplest)
typedef struct _DIBHeader {
    uint32_t hdrSize;
    uint16_t width;
    uint16_t height;
    uint16_t numColourPlanes;
    uint16_t bitsPerPixel;
} DIBHeader;

// BmpHeader is the file header followed by the DIB header
typedef struct _BmpHeader {
    FileHeader fileHdr;
    DIBHeader DIBHdr;
} BmpHeader;

// BmpFile contains the entire structure of a bitmap file
typedef struct _BmpFile {
    BmpHeader header;
    BmpImage image;
} BmpFile;

// Allow the compiler to do what it wants the rest of the time
#pragma pack(pop)

BmpHeader newBmpHeader(void);

unsigned int bitmapSize(void) {
    return sizeof(BmpHeader)+(BITS_PER_PIXEL*BMP_WIDTH*BMP_HEIGHT)/8;
}

void createBitmap(void* toAddress, BmpImage* image) {
    // In case some mistake is made with alignment...
    assert(sizeof(DIBHeader) == 12);
    assert(sizeof(FileHeader) == 14);
    
    BmpFile file;
    file.header = newBmpHeader();
    memcpy(&file.image, image, sizeof(BmpImage));
    memcpy(toAddress, &file, sizeof(BmpFile));
}

// given a width and height in pixels, a Bitmap header is returned
BmpHeader newBmpHeader(void) {
    BmpHeader bmpHdr;
    
    bmpHdr.fileHdr.magic1 = 'B';
    bmpHdr.fileHdr.magic2 = 'M';
    // Note the below assumes the width is a multiple of 4
    uint32_t filesize = sizeof(BmpHeader) + BITS_PER_PIXEL *
                                    BMP_WIDTH * BMP_HEIGHT / 8;
    bmpHdr.fileHdr.fileSize = littleUint32(filesize);
    // The creator fields shouldn't matter
    bmpHdr.fileHdr.imageOffset = littleUint32(sizeof(BmpHeader));
    
    bmpHdr.DIBHdr.hdrSize = littleUint32(sizeof(DIBHeader));
    bmpHdr.DIBHdr.width = littleUint16(BMP_WIDTH);
    bmpHdr.DIBHdr.height = littleUint16(BMP_HEIGHT);
    bmpHdr.DIBHdr.numColourPlanes = littleUint16(1);
    bmpHdr.DIBHdr.bitsPerPixel = littleUint16(BITS_PER_PIXEL);

    return bmpHdr;
}