// mandelbrot.c
// COMP1917 2012s1 Task 2
// Daniel Playfair Cal (dpca031, z3418174) &
// Natalie Wyburn (nlwy414, z3393694)
// Tutorial: Monday 12-Moog - Adrian Ratter
// 13/04/2012

// Webserver for exploring the Mandelbrot set in beautiful colours.

#include "pixelColor.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <netinet/in.h>
#include <unistd.h>
#include <math.h>

#define HTTP_PAGES_TO_SERVE         100
#define HTTP_PORT                  8174
#define HTTP_MAX_BACKLOG             10
#define HTTP_REQUEST_BUFFER_SIZE   1000
#define HTTP_REQUEST_TYPE_GET         0
#define HTTP_REQUEST_TYPE_UNKNOWN     1
#define HTTP_RESOURCE_BUFFER_SIZE   200

#define MIME_TYPE_BMP  0
#define MIME_TYPE_HTML 1

#define MANDELBROT_MAX_STEPS 256

// BMP_WIDTH and BMP_HEIGHT must be multiples of 4.
// BMP_BIT_DEPTH should always be 24
#define BMP_WIDTH              512
#define BMP_HEIGHT             512
#define BMP_BIT_DEPTH           24
#define BMP_COMPRESSION_NONE     0
#define BMP_PIXELS_PER_METRE  9001

// These are simply to differentiate between the two sockets more easily
typedef int ServerSocket;
typedef int ConnectionSocket;

/// Structs for creation of bitmap files:

// Struct to hold colour data for a pixel.
// The unusual order of colours is defined by the bitmap spec
typedef struct _BmpPixel {
    unsigned char blue;
    unsigned char green;
    unsigned char red;
} BmpPixel;

// BmpImage contains an array of pixels from which a bitmap can be made
typedef struct _BmpImage {
    BmpPixel pixels[BMP_HEIGHT][BMP_WIDTH];
} BmpImage;

// Prevents automatic padding on the structs which break the file format
#pragma pack(push, 1)

// Bitmap file header - the first part of the file with basic metadata
typedef struct _BmpFileHeader {
    char magic1;
    char magic2;
    uint32_t fileSize;
    uint16_t creator1;
    uint16_t creator2;
    uint32_t imageOffset;
} BmpFileHeader;

// DIB Header - comes after file header and defines the image properties
// This header is of the type BITMAPINFOHEADER
typedef struct _BmpDIBHeader {
    uint32_t hdrSize;
    int32_t width;
    int32_t height;
    uint16_t numColourPlanes;
    uint16_t bitsPerBmpPixel;
    uint32_t compression;
    uint32_t imageSize;
    int32_t horizontalRes;
    int32_t verticalRes;
    uint32_t numColours;
    uint32_t numImportantColours;
} BmpDIBHeader;

// BmpHeader is the file header followed by the DIB header
typedef struct _BmpHeader {
    BmpFileHeader fileHdr;
    BmpDIBHeader DIBHdr;
} BmpHeader;

// BmpFile contains the entire structure of a bitmap file
typedef struct _BmpFile {
    BmpHeader header;
    BmpImage image;
} BmpFile;

// Allow the compiler to pad out structs the rest of the time
#pragma pack(pop)


// Struct to store the http request type (GET/unknown) and resource path
typedef struct _HttpRequestInfo {
    int type;
    char resource[HTTP_RESOURCE_BUFFER_SIZE];
} HttpRequestInfo;

// To allow changing between float formats more easily
typedef double ComplexFloat;

// Struct to store a complex number in cartesian form
typedef struct _ComplexNumber {
    ComplexFloat Re;
    ComplexFloat Im;
} ComplexNumber;

// Struct to store the location of an image (x, y, zoom):
typedef struct _ImageLocation {
    ComplexNumber centre;
    int zoom;
} ImageLocation;

// Array storing mandelbrot steps for each pixel
typedef struct _StepsArray {
    int pixels[BMP_HEIGHT][BMP_WIDTH];
} StepsArray;


/// WEBSERVING PROTOTYPES:
// Make a server socket and bind it to the the first port available
// between startPort and endPort
ServerSocket makeServerSocket(int startPort, int endPort);
// Close an open server socket:
void closeServerSocket(ServerSocket socket);
// Wait for a connection on the specified server socket and return it:
ConnectionSocket waitForConnection(ServerSocket serverSocket);
// Close an open connection:
void closeConnectionSocket(ConnectionSocket socket);
// Return the next http connection socket on the specified server socket:
HttpRequestInfo getHttpRequestInfo(ConnectionSocket socket);
// Send an http response containing a bitmap file:
void serveBmp(ConnectionSocket socket, BmpFile* p_file);
// Send an http response containing an html string:
void serveHTML(ConnectionSocket socket, char* htmlString);

/// MANDELBROT SET PROTOTYPES:
// Make an index of the mandelbrot steps taken for each pixel:
StepsArray makeStepsArray(ImageLocation location);

/// ENDIAN PROTOTYPES:
// Ensure that integers are in little endian format for bitmap images:
uint16_t littleUint16(uint16_t input);
uint32_t littleUint32(uint32_t input);
int32_t littleInt32(int32_t input);

/// BITMAP FILE CREATION PROTOTYPES:
// Create a bitmap file in memory and returns a pointer to it:
BmpFile* p_createBitmapFile(void);
// Free the memory used by a bitmap file pointed to by p_file:
void destroyBitmapFile(BmpFile* p_file);

/// TOP_LEVEL FUNCTION PROTOTYPES:
// Serve an bmp image of the mandelbrot at the given location and zoom:
void servePicture(ConnectionSocket socket, ImageLocation location);
// Serve the mandelbrot browser script:
void serveHome(ConnectionSocket socket);
// Parse an http resource string to find image parameters:
ImageLocation parseBmpResourceString(char* httpResource);


int main(int argc, char *argv[]) {
    ServerSocket serverSocket;
    serverSocket = makeServerSocket(HTTP_PORT, HTTP_PORT + 7);
    
    int exitFlag = 0;
    while (exitFlag != 42) {
        ConnectionSocket connectionSocket;
        connectionSocket = waitForConnection(serverSocket);
        printf("Received connection\n");
        HttpRequestInfo requestInfo;
        requestInfo = getHttpRequestInfo(connectionSocket);
        if (requestInfo.type == HTTP_REQUEST_TYPE_GET) {
            if (requestInfo.resource[1] == 'X') {
                ImageLocation location;
                location = parseBmpResourceString(requestInfo.resource);
                printf("Serving bitmap: centre: %Lf%+Lfi, zoom: %d\n",
                                     (long double) location.centre.Re,
                                     (long double) location.centre.Im,
                                                       location.zoom);
                // cast the coordinates as long doubles so they will
                // work whether they are doubles or long doubles
                servePicture(connectionSocket, location);
            } else if (strcmp(requestInfo.resource, "/42") == 0) {
                printf("Reveived secret shutdown code\n");
                exitFlag = 42;
            } else {
                printf("Serving mandelbrot browser script\n");
                serveHome(connectionSocket);
            }
        } else {
            printf("Error: unknown http request type\n");
        }
        
        closeConnectionSocket(connectionSocket);
        printf("Connection closed.\n\n");
    }
    
    printf("Shutting down the server...\n");
    closeServerSocket(serverSocket);
    
    return EXIT_SUCCESS;
}

void servePicture(ConnectionSocket socket, ImageLocation location) {
    // create a pretty looking picture
    StepsArray stepsPerPixel;
    stepsPerPixel = makeStepsArray(location);
    BmpFile* p_file = p_createBitmapFile();
    int row, column;
    for (row = 0; row < BMP_HEIGHT; row++) {
        for (column = 0; column < BMP_WIDTH; column++) {
            p_file->image.pixels[row][column].red =
                    stepsToRed(stepsPerPixel.pixels[row][column]);
            p_file->image.pixels[row][column].green =
                    stepsToGreen(stepsPerPixel.pixels[row][column]);
            p_file->image.pixels[row][column].blue =
                    stepsToBlue(stepsPerPixel.pixels[row][column]);
        }
    }
    // Now send the bitmap in a http response
    serveBmp(socket, p_file);
    destroyBitmapFile(p_file);
}

void serveHome(ConnectionSocket socket) {
    char* htmlString = "<html>\n"
    "<script src=\"https://openlearning.cse.unsw.edu.au"
    "/site_media/viewer/tile.min.js\" ></script>\n"
    "</html>";
    serveHTML(socket, htmlString);
}

ImageLocation parseBmpResourceString(char* resourceString) {
    ImageLocation result;
    // The mandelbrowser script only uses doubles
    double realPart, imaginaryPart;
    int numbersRead = sscanf(resourceString,
                             "/X-%lf-%lf-%d.bmp",
                             &realPart,
                             &imaginaryPart,
                             &result.zoom);
    if (numbersRead == 3) {
        // If long doubles are used, the casting below is necessary.
        result.centre.Re = (ComplexFloat) realPart;
        result.centre.Im = (ComplexFloat) imaginaryPart;
    } else {
        printf("Error: only %d numbers in image request\n", numbersRead);
        result.centre.Re = 0;
        result.centre.Im = 0;
        result.zoom = 7;
    }
    return result;
}

/// WEBSERVING FUNCTIONS:

// wait for a browser to request a connection,
// returns the socket on which the conversation will take place
ConnectionSocket waitForConnection(ServerSocket serverSocket) {
    // listen for a connection
    listen(serverSocket, HTTP_MAX_BACKLOG);
    
    // accept the connection
    struct sockaddr_in clientAddress;
    socklen_t clientLen = sizeof(clientAddress);
    ConnectionSocket connectionSocket = accept(serverSocket,
                      (struct sockaddr *) &clientAddress, &clientLen);

    if (connectionSocket < 0) {
        printf("Error: couldn't accept connection.\n");
    }
    // error on accept
    
    return connectionSocket;
}


// start the server listening on the first available port between
// startPort and endPort
ServerSocket makeServerSocket(int startPort, int endPort) {
    // create socket
    ServerSocket serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (serverSocket < 0) {  // error opening socket
        printf("Error: could not create server socket\n");
        abort();
    }
    
    int bindSuccess = -1;
    int currentPort;
    for (currentPort = startPort; bindSuccess == -1 &&
                    currentPort <= endPort; currentPort++) {
        // bind socket to listening port
        struct sockaddr_in serverAddress;
        memset(&serverAddress, 0, sizeof (serverAddress));
        serverAddress.sin_family      = AF_INET;
        serverAddress.sin_addr.s_addr = INADDR_ANY;
        serverAddress.sin_port        = htons(currentPort);

        // let the server start immediately after a previous shutdown
        int optionValue = 1;
        setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR,
                    &optionValue, sizeof(int));
        bindSuccess = bind(serverSocket,
                           (struct sockaddr *) &serverAddress,
                           sizeof (serverAddress));
    }
    if (bindSuccess == 0) {
        printf("Access server at http://localhost:%d/\n",
               currentPort - 1);
    } else {
        printf("Error: could not start the server on any port from ");
        printf("%d to %d.\n", startPort, endPort);
        abort();
    }
    return serverSocket;
}

// Processes an http request string and returns a HttpRequestInfo struct
// containing the request type and the resource requested
HttpRequestInfo processHttpRequest(char* request) {
    HttpRequestInfo result;
    char* httpGetString = "GET";
    char* locationGet = strstr(request, httpGetString);
    
    if (locationGet != request) { // request doesn't start with GET
        result.type = HTTP_REQUEST_TYPE_UNKNOWN;
        result.resource[0] = '\0';
    } else {
        char* startResource = strchr(request, '/');
        char* endResource = strchr(startResource, ' ');
        if (endResource != NULL) {
            *endResource = '\0'; // terminate the string there
        } // endResource will be null if a space was not found - ie
          // there was nothing after the resource in the request
        printf("Requested resource: %s\n", startResource);
        result.type = HTTP_REQUEST_TYPE_GET;
        strncpy(result.resource,
                startResource,
                HTTP_RESOURCE_BUFFER_SIZE - 1);
    }
    return result;
}

char* p_createHttpHeader(int mimeType) {
    char* part1 = "HTTP/1.0 200 OK\r\n";
    char* part2 = "Content-Type: ";
    char* part3;
    if (mimeType == MIME_TYPE_BMP) {
        part3 = "image/bmp";
    } else {
        part3 = "text/html";
    }
    char* part4 = "\r\n\r\n";
    
    char* header = malloc(strlen(part1) +
                          strlen(part2) +
                          strlen(part3) +
                          strlen(part4) + 1);
    strcpy(header, part1);
    strcat(header, part2);
    strcat(header, part3);
    strcat(header, part4);
    return header;
}

void destroyHttpHeader(char* p_header) {
    free(p_header);
}

void serveData(ConnectionSocket socket,
               int mimeType,
               void* p_data,
               unsigned int size) {
    char* header = p_createHttpHeader(mimeType);
    
    int bytesWritten;
    bytesWritten = write(socket, header, strlen (header));
    if (bytesWritten <= 0) {
        printf("Error: could not write http header to socket\n");
    }
    destroyHttpHeader(header);
    
    bytesWritten = 0;
    bytesWritten = write(socket, p_data, size);
    if (bytesWritten <= 0) {
        printf("Error: could not write http page to socket\n");
    }
}

void serveBmp(ConnectionSocket socket, BmpFile* p_file) {
    serveData(socket, MIME_TYPE_BMP, p_file, sizeof(BmpFile));
}

void serveHTML(ConnectionSocket socket, char* htmlString) {
    serveData(socket, MIME_TYPE_HTML, htmlString, strlen(htmlString));
}

HttpRequestInfo getHttpRequestInfo(ConnectionSocket socket) {
    char request[HTTP_REQUEST_BUFFER_SIZE + 1];
    // read the request sent by the browser
    int bytesRead;
    bytesRead = read(socket, request, HTTP_REQUEST_BUFFER_SIZE);
    request[bytesRead] = '\0'; // terminate string
    if (bytesRead < 0) {
        printf("Error: received a request with no data\n");
    }
    HttpRequestInfo requestInfo = processHttpRequest(request);
    return requestInfo;
}

void closeConnectionSocket(ConnectionSocket socket) {
    close(socket);
}

void closeServerSocket(ServerSocket socket) {
    close(socket);
}


/// MANDELBROT AND COMPLEX NUMBER FUNCTIONS

// Find the square modulus of a given complex number:
ComplexFloat complexSquareModulus(ComplexNumber input) {
    return input.Re*input.Re + input.Im*input.Im;
}

// Square a complex number:
ComplexNumber complexSquare(ComplexNumber input) {
    ComplexNumber result;
    result.Re = input.Re * input.Re - input.Im * input.Im;
    result.Im = 2 * input.Re * input.Im;
    return result;
}

// Sum two complex numbers:
ComplexNumber complexSum(ComplexNumber input1, ComplexNumber input2) {
    ComplexNumber result;
    result.Re = (input1.Re + input2.Re);
    result.Im = (input1.Im + input2.Im);
    return result;
}

// Find the number of iterations of the mandelbrot function necessary
// to make the result's modulus >= 2 (proving it is not part of the
// set). Returns MANDELBROT_MAX_STEPS if the result's modulus remained
// < 2:
int mandelbrotSteps(ComplexNumber input) {
    ComplexNumber currentScore;
    currentScore.Re = 0;
    currentScore.Im = 0;
    int steps;
    for (steps = 0; complexSquareModulus(currentScore) < 4 &&
                        steps < MANDELBROT_MAX_STEPS; steps++) {
        currentScore = complexSquare(currentScore);
        currentScore = complexSum(currentScore, input);
    }
    return steps;
}

// Find the distance between pixels on the complex plane:
ComplexFloat pixelSpacing(int zoomLevel) {
    return pow(2.0, -zoomLevel);
}

// Find the location on the complex plane of the image's top left corner
ComplexNumber topLeftCorner(ImageLocation location) {
    ComplexNumber result = location.centre;
    result.Re -= (1.0/2) * BMP_WIDTH * pixelSpacing(location.zoom);
    result.Im += (1.0/2) * BMP_HEIGHT * pixelSpacing(location.zoom);
    return result;
}

// Find the mandelbrot steps taken for each pixel in the image:
StepsArray makeStepsArray(ImageLocation location) {
    StepsArray result;
    ComplexNumber topLeft = topLeftCorner(location);
    ComplexFloat distance = pixelSpacing(location.zoom);
    
    ComplexNumber currentPoint = topLeft;
    int row, column;
    for (row = 0; row < BMP_HEIGHT; row++) {
        currentPoint.Re = topLeft.Re;
        for (column = 0; column < BMP_WIDTH; column++) {
            result.pixels[row][column] = mandelbrotSteps(currentPoint);
            currentPoint.Re += distance;
        }
        currentPoint.Im -= distance;
    }
    return result;
}


/// ENDIAN FUNCTIONS

// Determine whether the host system is little or big endian:
int hostEndian() {
    uint16_t integer = 0xFF11; // MSB = 0xFF, LSB = 0x11
    char* p_firstByte = (char*) &integer;
    if (*p_firstByte == 0x11) {
        return LITTLE_ENDIAN;
    } else {
        return BIG_ENDIAN;
    }
}

// If necessary, convert a uint16_t into little endian format:
uint16_t littleUint16(uint16_t input) {
    uint16_t output;
    if (hostEndian() == LITTLE_ENDIAN) {
        output = input;
    } else {
        char* p_inputBytes = (char*) &input;
        char* p_outputBytes = (char*) &output;
        p_outputBytes[0] = p_inputBytes[1];
        p_outputBytes[1] = p_inputBytes[0];
    }
    return output;
}

// If necessary, convert a uint32_t into little endian format:
uint32_t littleUint32(uint32_t input) {
    uint32_t output;
    if (hostEndian() == LITTLE_ENDIAN) {
        output = input;
    } else {
        char* p_inputBytes = (char*) &input;
        char* p_outputBytes = (char*) &output;
        p_outputBytes[0] = p_inputBytes[3];
        p_outputBytes[1] = p_inputBytes[2];
        p_outputBytes[2] = p_inputBytes[1];
        p_outputBytes[3] = p_inputBytes[0];
    }
    return output;
}

// If necessary, convert a int32_t into little endian format:
int32_t littleInt32(int32_t input) {
    int32_t output;
    if (hostEndian() == LITTLE_ENDIAN) {
        output = input;
    } else {
        char* p_inputBytes = (char*) &input;
        char* p_outputBytes = (char*) &output;
        p_outputBytes[0] = p_inputBytes[3];
        p_outputBytes[1] = p_inputBytes[2];
        p_outputBytes[2] = p_inputBytes[1];
        p_outputBytes[3] = p_inputBytes[0];
    }
    return output;
}


/// BITMAP FUNCTIONS:

void setBmpFileHeaderValues(BmpFileHeader* p_fileHdr) {
    p_fileHdr->magic1 = 'B';
    p_fileHdr->magic2 = 'M';
    // Note the below assumes the width is a multiple of 4
    uint32_t filesize = sizeof(BmpHeader) +
    BMP_BIT_DEPTH * BMP_WIDTH * BMP_HEIGHT / 8;
    p_fileHdr->fileSize = littleUint32(filesize);
    // The creator fields shouldn't matter
    p_fileHdr->imageOffset = littleUint32(sizeof(BmpHeader));
}

void setBmpDIBHeaderValues(BmpDIBHeader* p_DIBHdr) {
    p_DIBHdr->hdrSize = littleUint32(sizeof(BmpDIBHeader));
    p_DIBHdr->width = littleInt32(BMP_WIDTH);
    p_DIBHdr->height = littleInt32(-BMP_HEIGHT);
    // A negative height value makes the image load top down
    p_DIBHdr->numColourPlanes = littleUint16(1);
    p_DIBHdr->bitsPerBmpPixel = littleUint16(BMP_BIT_DEPTH);
    p_DIBHdr->compression = littleUint32(BMP_COMPRESSION_NONE);
    p_DIBHdr->imageSize = littleUint32(0); // valid (no compression)
    p_DIBHdr->horizontalRes = littleInt32(BMP_PIXELS_PER_METRE);
    p_DIBHdr->horizontalRes = littleInt32(BMP_PIXELS_PER_METRE);
    p_DIBHdr->numColours = littleUint32(0); // 0 = default to 2^n
    p_DIBHdr->numImportantColours = littleUint32(0);// All are important
}

// Make a bitmap header
void setBmpHeaderValues(BmpHeader* p_header) {
    setBmpFileHeaderValues(&p_header->fileHdr);
    setBmpDIBHeaderValues(&p_header->DIBHdr);
}

// Create a bitmap file in memory (pixel contents undefined)
// Returns a pointer to the file
BmpFile* p_createBitmapFile(void) {
    // In case some mistake is made with struct padding...
    if (sizeof(BmpDIBHeader) != 40 || sizeof(BmpFileHeader) != 14) {
        printf("Error: bitmap header structs are the wrong size\n");
        abort();
    }
    BmpFile* p_file = malloc(sizeof(BmpFile));
    setBmpHeaderValues(&p_file->header);
    return p_file;
}

// Destroy a bitmap file in memory, freeing up the memory it was in
void destroyBitmapFile(BmpFile* p_file) {
    free(p_file);
}