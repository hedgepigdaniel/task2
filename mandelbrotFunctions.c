/*
 *  mandelbrot.c
 *  mandelbrot
 *
 *  Created by Natalie Wyburn on 12/04/12.
 *  Copyright. All rights reserved.
 *
 */

#include "mandelbrot.h"
#include <math.h>

//Finds the modulus of a given complex number
double complexModulus(ComplexNumber number) {

	return sqrt(number.Re*number.Re + number.Im*number.Im);
}

//Squares a complex number.
ComplexNumber squareComplexNumber(ComplexNumber input) {
	
	
	ComplexNumber result;
	result.Re=(input.Re*input.Re-input.Im*input.Im);
	result.Im=(2*input.Re*input.Im);
	
	return result;
}

//Sum of two complex numbers.
ComplexNumber sumComplexNumbers(ComplexNumber input1, ComplexNumber input2) {
	
	ComplexNumber result;
	result.Re=(input1.Re+input2.Re);
	result.Im=(input1.Im+input2.Im);
	
	return result;
}
	
//Checks if a complex number is a member of the Mandelbrot set.
int mandelbrotIterations(ComplexNumber input) {

	ComplexNumber currentScore;
	currentScore.Re=0;
	currentScore.Im=0;
	
	int iterations;
	for (iterations=0; complexModulus(currentScore)<2 && iterations<256; iterations++) {
		currentScore=squareComplexNumber(currentScore);
		currentScore=sumComplexNumbers(currentScore, input);
	}
	
	return iterations;
}

//Shows the distance between pixels at a certain zoom level
double distanceBetweenPixels(int zoomLevel) {
	
	return pow(2.0,-zoomLevel);
	
}

//Finds top left corner of image, given zoom level and centre point.
ComplexNumber topLeftCorner(ComplexNumber centre, int zoomLevel) {
	
	ComplexNumber result=centre;
	result.Re-=((1.0/2)*BMP_WIDTH)*distanceBetweenPixels(zoomLevel);
	result.Im+=((1.0/2)*BMP_HEIGHT)*distanceBetweenPixels(zoomLevel);
	
	return result;
	
}

//
IterationsImage makeIterationsImage(ComplexNumber centre, int zoomLevel) {
	
	IterationsImage result;
	ComplexNumber topLeft=topLeftCorner(centre, zoomLevel);
	double distance=distanceBetweenPixels(zoomLevel);
	
	ComplexNumber currentPoint = topLeft;
	
	int row;
	int column;
	
	for  (row=0; row<BMP_HEIGHT; row++) {
		for (column=0; column<BMP_WIDTH; column++) {
			result.pixels [row][column] =
                            mandelbrotIterations(currentPoint);
			
			currentPoint.Re+=distance;
		}
		currentPoint.Im-=distance;
	}
	
	return result;
}


