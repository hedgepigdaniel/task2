/*
 * webserver.c
 * Daniel Playfair Cal & Natalie Wyburn
 * provides functions to serve data over http
 */

#include "webserver.h"

#include <netinet/in.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#define HTTP_RESPONSE_HEADER_BUFFER_SIZE 200

// wait for a browser to request a connection,
// returns the socket on which the conversation will take place
ConnectionSocket waitForConnection (ServerSocket serverSocket) {
    // listen for a connection
    listen (serverSocket, MAX_CONNECTION_BACKLOG);
    
    // accept the connection
    struct sockaddr_in clientAddress;
    socklen_t clientLen = sizeof (clientAddress);
    ConnectionSocket connectionSocket = accept(serverSocket,
                (struct sockaddr *) &clientAddress, &clientLen);
    
    assert (connectionSocket >= 0);
    // error on accept
    
    return connectionSocket;
}


// start the server listening on the specified port number
ServerSocket makeServerSocket (int portNumber) {
    // create socket
    ServerSocket serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    assert (serverSocket >= 0);  // error opening socket
    
    // bind socket to listening port
    struct sockaddr_in serverAddress;
    memset(&serverAddress, 0, sizeof (serverAddress));
    serverAddress.sin_family      = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port        = htons(portNumber);
    
    // let the server start immediately after a previous shutdown
    int optionValue = 1;
    setsockopt (serverSocket, SOL_SOCKET, SO_REUSEADDR,
                                            &optionValue, sizeof(int));
    int bindSuccess = bind(serverSocket,
           (struct sockaddr *) &serverAddress, sizeof (serverAddress));
    assert (bindSuccess >= 0);
    // if this assert fails wait a short while to let the operating
    // system clear the port before trying again
    
    return serverSocket;
}

// Processes an http request string and returns a HttpRequestInfo struct
// containing the request type and the resource requested
HttpRequestInfo processHttpRequest(char* request) {
    HttpRequestInfo result;
    char* httpGetString = "GET";
    char* locationGet = strstr(request, httpGetString);
    
    if (locationGet != request) { // request doesn't start with GET
        result.type = REQUEST_TYPE_UNKNOWN;
        result.resource = NULL;
    } else {
        char* resource = strchr(request, '/') + 1;
        char* endResource = strchr(resource, ' ');
        *endResource = '\0'; // terminate the string there
        result.resource = resource;
    }
    return result;     
}

char* httpHeader(int mimeType) {
    char* part1 = "HTTP/1.0 200 OK\r\n";
    char* part2 = "Content-Type: ";
    char* part3;
    if (mimeType == MIME_BMP) {
        part3 = "image/bmp";
    } else {
        part3 = "text/html";
    }
    char* part4 = "\r\n\r\n";

    char* header = malloc(strlen(part1) + strlen(part2) +
                          strlen(part3) + strlen(part4) + 1);
    strcpy(header, part1);
    strcat(header, part2);
    strcat(header, part3);
    strcat(header, part4);

    return header;
}

void serveData (ConnectionSocket socket, int mimeType, void* data,
                unsigned int size) {
    char* header = httpHeader(mimeType);
    
    int bytesWritten;
    bytesWritten = write(socket, header, strlen (header));
    assert(bytesWritten > 0);
    
    bytesWritten = 0;
    bytesWritten = write(socket, data, size);
    assert(bytesWritten > 0);
}

void serveBmp(ConnectionSocket socket, void* data, unsigned int size) {
    serveData(socket, MIME_BMP, data, size);
}

void serveHTML(ConnectionSocket socket, void* data, unsigned int size) {
    serveData(socket, MIME_HTML, data, size);
}


HttpRequestInfo getHttpRequestInfo(ConnectionSocket socket) {
    char request[REQUEST_BUFFER_SIZE];
    // read the request sent by the browser
    int bytesRead;
    bytesRead = read(socket, request, (sizeof request)-1);
    assert(bytesRead >= 0);
    
    HttpRequestInfo requestInfo = processHttpRequest(request);
    return requestInfo;
}

void closeConnectionSocket(ConnectionSocket socket) {
    close(socket);
}

void closeServerSocket(ServerSocket socket) {
    close(socket);
}