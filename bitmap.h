/*
 * bitmap.h
 * COMP1917 task 2
 * Daniel Playfair Cal and Natalie Wyburn
 * 03/04/2012
 */

/*
 * This file defines functions for creating a bmp file from an array of
 * RGB values
 */

// BMP_WIDTH and BMP_HEIGHT must be multiples of 4.
// BITS_PER_PIXEL should always be 24
#define BITS_PER_PIXEL    24
#define BMP_WIDTH        512
#define BMP_HEIGHT       512

// Struct to hold colour data for a pixel.
// The unusual order of colours is defined by the bitmp spec
typedef struct _Pixel {
    unsigned char blue;
    unsigned char green;
    unsigned char red;
} Pixel;

// BmpImage contains an array of pixels from which a bitmap can be made
typedef struct _BmpImage {
    Pixel pixels [BMP_HEIGHT] [BMP_WIDTH];
} BmpImage;



// createBitmap writes a bitmap file to toAddress given a BmpImage
void createBitmap(void* toAddress, BmpImage* image);

// bitmapSize returns the size in bytes of a bitmap file returned by
// createBitmap.
unsigned int bitmapSize(void);