#include <stdio.h>
#include <stdlib.h>

int main (int argc, char* argv[]) {
    printf("sizeof(float): %d\n", (int) sizeof(float));
    printf("sizeof(double): %d\n", (int) sizeof(double));
    printf("sizeof(long double): %d\n", (int) sizeof(long double));
    return EXIT_SUCCESS;
}