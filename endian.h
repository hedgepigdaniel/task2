/*
 * endian.h
 * Daniel Playfair Cal
 * Provides functions for formatting variables in a specific endian
 */

#include <stdint.h>

// Convert the input into little endian format where necessary
uint16_t littleUint16(uint16_t input);
uint32_t littleUint32(uint32_t input);