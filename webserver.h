/*
 * webserver.h
 * COMP1917 task 2
 * Daniel Playfair Cal and Natalie Wyburn
 * 03/04/2012
 */

/*
 * This file defines functions for serving files over http
 */

#define DEFAULT_PORT 8174
#define MAX_CONNECTION_BACKLOG 10
#define REQUEST_BUFFER_SIZE 1000

#define REQUEST_TYPE_GET     0
#define REQUEST_TYPE_UNKNOWN 1

#define MIME_BMP  0
#define MIME_HTML 1

typedef int ServerSocket;
typedef int ConnectionSocket;

typedef struct _HttpRequestInfo {
    int type;
    char* resource;
} HttpRequestInfo;

// Make a server socket and bind it to the specified port
ServerSocket makeServerSocket (int portNumber);

// Close an open server socket
void closeServerSocket(ServerSocket socket);

// Wait for a connection on the specified server socket and return it
ConnectionSocket waitForConnection (ServerSocket serverSocket);

// Close an open connection
void closeConnectionSocket(ConnectionSocket socket);

// Wait for an http request on the specified server socket and return it
HttpRequestInfo getHttpRequestInfo(ConnectionSocket socket);

// Send the specified type of data in a http response
void serveData (ConnectionSocket socket, int mimeType, void* data,
                                            unsigned int numBytes);

// Send a http response containing a bitmap file
void serveBmp(ConnectionSocket socket, void* data, unsigned int size);

// Send an http response containing html
void serveHTML(ConnectionSocket socket, void* data, unsigned int size);