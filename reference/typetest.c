/*
 * typetest.c
 * Daniel Playfair Cal
 * 08/04/2012
 * program to print the size of various types
 */

#include <stdio.h>
#include <stdlib.h>

int main (int argc, char* argv []) {
    printf("sizeof(char): %d bytes\n", (int) sizeof(char));
    printf("sizeof(short): %d bytes\n", (int) sizeof(short));
    printf("sizeof(int): %d bytes\n", (int) sizeof(int));
    printf("sizeof(long): %d bytes\n", (int) sizeof(long));

    return EXIT_SUCCESS;
}