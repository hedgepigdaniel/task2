/*
 * endian.h
 * Daniel Playfair Cal
 * Provides functions for formatting variables in a specific endian
 */

#include "endian.h"

#include <assert.h>

#define LITTLE_ENDIAN 0
#define BIG_ENDIAN    1

// Determine whether the host system is little or big endian
int hostEndian() {
    uint16_t integer = 0xFF11; // MSB = 0xFF, LSB = 0x11
    char* p_firstByte = (char *) &integer;
    if (*p_firstByte == 0x11) {
        return LITTLE_ENDIAN;
    } else {
        return BIG_ENDIAN;
    }
}

uint16_t littleUint16(uint16_t input) {
    uint16_t output;
    if (hostEndian() == LITTLE_ENDIAN) {
        output = input;
    } else {
        char* p_inputBytes = (char*) &input;
        char* p_outputBytes = (char *) &output;
        p_outputBytes[0] = p_inputBytes[1];
        p_outputBytes[1] = p_inputBytes[0];
    }
    return output;
}

uint32_t littleUint32(uint32_t input) {
    uint32_t output;
    if (hostEndian() == LITTLE_ENDIAN) {
        output = input;
    } else {
        char* p_inputBytes = (char*) &input;
        char* p_outputBytes = (char *) &output;
        p_outputBytes[0] = p_inputBytes[3];
        p_outputBytes[1] = p_inputBytes[2];
        p_outputBytes[2] = p_inputBytes[1];
        p_outputBytes[3] = p_inputBytes[0];
    }
    return output;
}