// pixelColor.c
// COMP1917 2012s1 Task 2
// Daniel Playfair Cal (dpca031, z3418174) &
// Natalie Wyburn (nlwy414, z3393694)
// Tutorial: Monday 12-Moog - Adrian Ratter
// 13/04/2012

// This file contains functions for determining the colour of each pixel
// depending on the number of steps required to determine whether the
// point is in the mandelbrot set.

// each colour is given a value from 0-255. the value of each colour is
// 255 is a defined range (given as a proportion of the maximum number
// of steps). outside of this range, the value decreases as a linear
// function of its distance from the peak range, such taht it is 0 when
// it is peak range away form the peak range. If the peak ranges are
// adjacent for each primary colour, the overall colour will fade
// smoothly through bright colours (and then to black, after the last
// colour). /When steps = 255 (ie the point did not escape) the colour
// is black.

#include "pixelColor.h"

// Note that this constant is also defined in mandelbrot.c
#define MANDELBROT_MAX_STEPS 256

// These constants define the peak range for each colour.
// The values make the colour fade from black -> blue -> green -> red
#define RED_PEAK_START      (1 * MANDELBROT_MAX_STEPS / 4)
#define RED_PEAK_END        (2 * MANDELBROT_MAX_STEPS / 4)

#define GREEN_PEAK_START    (2 * MANDELBROT_MAX_STEPS / 4)
#define GREEN_PEAK_END      (3 * MANDELBROT_MAX_STEPS / 4)

#define BLUE_PEAK_START     (3 * MANDELBROT_MAX_STEPS / 4)
#define BLUE_PEAK_END       (4 * MANDELBROT_MAX_STEPS / 4)


static int max(int x, int y) {
    if (x > y) {
        return x;
    } else {
        return y;
    }
}

// Given a number of steps and the startpoint and endpoint of the peak
// range of the colour, this function returns its value (0-255)
static unsigned char peakRangeToValue(int steps, int start, int end) {
    int range = end - start;
    int result;
    if (steps < start) {
        int distanceFromStart = start - steps;
        result = max(0, ((range - distanceFromStart) * 255) / range);
    } else if (steps > start) {
        int distanceFromEnd = steps - end;
        result = max(0, ((range - distanceFromEnd) * 255) / range);
    } else {
        result = 255;
    }
    return result;
}

// Returns the red value given the steps taken
unsigned char stepsToRed(int steps) {
    unsigned char red;
    if (steps == MANDELBROT_MAX_STEPS) {
        red = 0;
    } else {
        red = peakRangeToValue(steps, RED_PEAK_START, RED_PEAK_END);
    }
    return red;
}

// Returns the green value given the steps taken
unsigned char stepsToGreen(int steps) {
    unsigned char green;
    if (steps == MANDELBROT_MAX_STEPS) {
        green = 0;
    } else {
        green = peakRangeToValue(steps, GREEN_PEAK_START, GREEN_PEAK_END);
    }
    return green;
}

// Returns the blue value given the steps taken
unsigned char stepsToBlue(int steps) {
    unsigned char blue;
    if (steps == MANDELBROT_MAX_STEPS) {
        blue = 0;
    } else {
        blue = peakRangeToValue(steps, BLUE_PEAK_START, BLUE_PEAK_END);
    }
    return blue;
}
