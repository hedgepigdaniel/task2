/*
 * task2.c
 * Daniel Playfair Cal & Natalie Wyburn
 * 12/04/2012
 * COMP1917 task2
 */

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "webserver.h"
//#include "bitmap.h"
#include "mandelbrot.h"

#define PAGES_TO_SERVE 100

    

void servePicture(ConnectionSocket socket);
void serveHome (ConnectionSocket socket);


int main (int argc, char *argv[]) {
    ServerSocket serverSocket = makeServerSocket (DEFAULT_PORT);
    printf ("Access server at http://localhost:%d/\n", DEFAULT_PORT);
    
    int numServed;
    for (numServed = 0; numServed < PAGES_TO_SERVE; numServed++) {
        printf("Served %d pages so far\n", numServed);
        
        ConnectionSocket connectionSocket;
        connectionSocket = waitForConnection(serverSocket);
        HttpRequestInfo requestInfo;
        requestInfo = getHttpRequestInfo(connectionSocket);
        
        if (requestInfo.resource[0] == 'X') {
            printf("Serving bitmap...\n");
            servePicture(connectionSocket);
            //TODO: serve the right picture.
        } else {
            printf("Serving homepage...\n");
            serveHome(connectionSocket);
        }
        
        closeConnectionSocket(connectionSocket);
    }
    
    printf ("Shutting down the server...\n");
    closeServerSocket(serverSocket);
    
    return EXIT_SUCCESS;
}

void servePicture(ConnectionSocket socket) {
    // create a pretty looking picture
    ComplexNumber centre = {0,0};
    IterationsImage iterationsPerPixel;
    iterationsPerPixel = makeIterationsImage(centre, 7);
    BmpImage image;
    int row, column;
    for (row = 0; row < BMP_HEIGHT; row++) {
        for (column = 0; column < BMP_WIDTH; column++) {
            image.pixels[row][column].red =
                    iterationsPerPixel.pixels[row][column];
            image.pixels[row][column].green =
                    iterationsPerPixel.pixels[row][column];
            image.pixels[row][column].blue =
                    iterationsPerPixel.pixels[row][column];
        }
    }
    
    char* bmpFile = malloc(bitmapSize());
    createBitmap(bmpFile, &image);
    
    // Now send the bitmap in a http response
    serveBmp(socket, bmpFile, bitmapSize());
}

void serveHome (ConnectionSocket socket) {
    char* content = "<html>\n"
    "<script src=\"https://openlearning.cse.unsw.edu.au"
    "/site_media/viewer/tile.min.js\" ></script>\n"
    "</html>";
    serveHTML(socket, content, strlen(content));
}