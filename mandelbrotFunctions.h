/*
 *  mandelbrot.h
 *  mandelbrot
 *
 *  Created by Natalie Wyburn on 12/04/12.
 *  Copyright. All rights reserved.
 *
 */

#include "bitmap.h"

int mandelbrotIterations(ComplexNumber input);

IterationsImage makeIterationsImage(ComplexNumber centre, int
zoomLevel);