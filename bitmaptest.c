/*
 * bitmaptest.c
 * Daniel Playfair Cal
 * 04/04/2012
 * simple test for bitmap.h/c
 */

/*
 * This program creates a bitmap image called testImage and colours it's
 * pixels according to a simple rule, which creates a nice image fading
 * from blue to yellow through grey (at least as long as the dimensions
 * are 512x512. It prints the dimensions, pixel array size, filesize,
 * and size of each pixel. It then writes the file to "test.bmp" in the
 * directory it runs from.
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "bitmap.h"

int main(int argc, char* argv[]) {
    BmpImage testImage;

    // create a pretty looking array of pixels
    int row, column;
    for (row = 0; row < BMP_HEIGHT; row++) {
        for (column = 0; column < BMP_WIDTH; column++) {
            testImage.pixels[row][column].red = row/2;
            testImage.pixels[row][column].green = row/2;
            testImage.pixels[row][column].blue = column/2;
        }
    }

    // Print out basic information about the file
    printf("Bitmap library tester\n");
    printf("Dimensions (WxH): %dx%d\n", BMP_WIDTH, BMP_HEIGHT);
    printf("File size (bytes): %d\n", bitmapSize());
    printf("sizeof(Pixel): %d\n", (int)sizeof(Pixel));
    printf("sizeof(BmpImage): %d\n", (int)sizeof(BmpImage));

    // create the bitmap file in testBitmap
    char* testBitmap = malloc(bitmapSize());
    createBitmap(testBitmap, &testImage);

    // Write the file to "test.bmp"
    FILE *fileHandle;
    fileHandle = fopen("test.bmp", "w"); // Open file for writing
    assert(fileHandle != NULL); //Make sure we succeeded
    fwrite(testBitmap, bitmapSize(), 1, fileHandle);
    assert(fclose(fileHandle) == 0);
    
    return EXIT_SUCCESS;
}